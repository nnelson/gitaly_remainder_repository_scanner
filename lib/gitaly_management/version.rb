# encoding: utf-8

module GitalyManagement
  module RepositoryScanner
    VERSION = ['1.0']
    GEM_RELEASE = 'r01'

    def self.version(release = GEM_RELEASE)
      VERSION << release unless release.nil?
      VERSION.join('.').freeze
    end
  end
end
