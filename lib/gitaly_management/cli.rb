# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'optparse'

require 'gitaly_management/version'

# GitalyManagement module
module GitalyManagement
  # CommandLineSupport module
  module CommandLineSupport
    # OptionsParser class
    class OptionsParser
      Fields = %i[banner dry_run command sum_disk_space verbose version help].freeze
      attr_reader :parser, :options

      def initialize(defaults = {})
        @parser = OptionParser.new
        @options = defaults.dup
        Fields.each { |method_name| self.method(method_name).call if self.respond_to?(method_name) }
      end

      def banner
        @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options]"
        @parser.separator ''
        @parser.separator 'Options:'
      end

      def dry_run
        description = 'Show what would have been done; default: yes'
        @parser.on('-d', '--dry-run=[yes/no]', description) do |dry_run|
          @options[:dry_run] = !dry_run.match?(/^(no|false)$/i)
        end
      end

      def command
        @parser.on('--command=command', 'Command to invoke on a leftover git repository') do |str|
          @options[:command] = str
        end
      end

      def sum_disk_space
        @parser.on('--sum-disk-space', 'Sum the disk space used by leftover repositories') do
          @options[:sum_disk_space] = true
        end
      end

      def verbose
        @parser.on('-v', '--verbose', 'Increase logging verbosity') do
          @options[:log_level] -= 1
        end
      end

      def version
        @parser.on_tail('-v', '--version', 'Show version') do
          puts "#{$PROGRAM_NAME} version #{GitalyManagement::RepositoryScanner.version}"
          exit
        end
      end

      def help
        @parser.on_tail('-?', '--help', 'Show this message') do
          puts @parser
          exit
        end
      end
    end
    # class OptionsParser

    def parse(args = ARGV, file_path = ARGF, defaults = {})
      opt = OptionsParser.new(defaults)
      args.push('-?') if args.empty?
      opt.parser.parse!(args)
      opt.options
    rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
           OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
      puts e.message
      puts opt.parser
      exit
    rescue OptionParser::AmbiguousOption => e
      abort e.message
    end
  end
  # module CommandLineSupport
end
# module GitalyManagement
