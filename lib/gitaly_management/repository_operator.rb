# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'fileutils'
require 'json'

require 'gitaly_management/helpers'
require 'gitaly_management/logging'

# GitalyManagement module
module GitalyManagement
  # RepositoryOperator class
  class RepositoryOperator
    include ::GitalyManagement::Helpers
    include ::GitalyManagement::Logging
    attr_reader :options, :leftovers_dir_path

    def initialize(options)
      @options = options
      log.level = @options[:log_level]
      init_paths
    end

    def init_paths
      @leftovers_dir_path = File.join(__dir__, options[:leftovers_dir_name])
      FileUtils.mkdir_p(leftovers_dir_path) unless File.directory?(leftovers_dir_path)
    end

    def timestamped_file_name(file_name)
      format(file_name, date: Time.now.strftime(options[:inventory_timestamp_format]))
    end

    def load_latest_leftovers
      leftovers_files = Dir.new(leftovers_dir_path).children
      return [] if leftovers_files.empty?

      latest_leftovers_file_path = File.join(leftovers_dir_path, leftovers_files.max)
      return [] unless File.exist?(latest_leftovers_file_path)

      IO.readlines(latest_leftovers_file_path, chomp: true)
    end

    def latest_leftovers
      leftovers = load_latest_leftovers.collect { |line| JSON.parse(line, symbolize_names: true) }
      log.info "Found #{leftovers.length} known leftover git repositories"
      leftovers
    end

    def update_status(iteration, total)
      status = format(
        options[:status],
        index: iteration,
        total: total,
        percent: ((iteration / total.to_f) * 100).round(2))
      status << "\n" if log.level == Logger::DEBUG || options[:dry_run]
      progress_log.info(status)
    end

    def each_with_status(enumerable)
      enumerable.each_with_index do |element, iteration|
        yield element
        update_status(iteration, enumerable.length)
      end
      $stdout.write("\n")
    end

    def operate(command, repositories)
      raise UserError, 'No command given; terminating' if command.nil? || command.empty?
      repositories_root_dir_path = options[:repositories_root_dir_path]
      each_with_status(repositories) do |repository|
        disk_path = repository[:disk_path]
        raise ApplicationError, "Encountered bad git repository disk path!" if disk_path.nil? || disk_path.empty?
        repository_path = File.join(repositories_root_dir_path, disk_path + '.git')
        cmd = [command, repository_path].join(' ')
        if options[:dry_run]
          log.info "[Dry-run] Would have executed command: #{cmd}"
        else
          log.debug "Executing command: #{cmd}"
          result = `#{cmd}`.strip
          yield result unless result.empty?
        end
      end
    end

    def sum_disk_space(command = 'du -xs')
      results = []
      operate(command, latest_leftovers) { |result| results << result }
      total_disk_space = results.sum do |result|
        disk_space_kilobytes = result.split(/\s+/).first.to_i
        disk_space_kilobytes * 1_000 # bytes
      end
      log.info "Estimated disk space which would be reclaimed by deleting leftover git " \
        "repositories: #{human_friendly_filesize(total_disk_space)}"
    end

    def run_command(command)
      results = []
      operate(command, latest_leftovers) { |result| results << result }
      results.each do |result|
        log.info result.to_s
      end
    end
  end
  # class RepositoryOperator
end
# module GitalyManagement
