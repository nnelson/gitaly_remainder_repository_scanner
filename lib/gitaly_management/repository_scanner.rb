#! /usr/bin/env ruby
# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'lib/cli'
require 'lib/config'
require 'lib/helpers'
require 'lib/logging'
require 'lib/repository_operator'

begin
  require '/opt/gitlab/embedded/service/gitlab-rails/config/environment.rb'
rescue LoadError => e
  warn "WARNING: #{e.message}"
end

# GitalyManagement module
module GitalyManagement
  # RepositoryScanner class
  class RepositoryScanner
    include ::GitalyManagement::Config
    include ::GitalyManagement::Logging
    include ::GitalyManagement::CommandLineSupport

    def main(args = parse(ARGV, ARGF, DEFAULTS))
      dry_run_notice if args[:dry_run]
      operator = ::GitalyManagement::RepositoryOperator.new(args)
      return operator.sum_disk_space if args[:sum_disk_space]
      operator.run_command(args[:command])
    rescue StandardError => e
      log.error("Unexpected error: #{e}")
      e.backtrace.each { |t| log.error(t) }
      abort
    rescue SystemExit
      exit
    rescue Interrupt => e
      $stderr.write "\r\n#{e.class}\n"
      $stderr.flush
      $stdin.echo = true
      exit 0
    end
  end
  # class RepositoryScanner
end
# module GitalyManagement

GitalyManagement::RepositoryScanner.new.main if $PROGRAM_NAME == __FILE__
