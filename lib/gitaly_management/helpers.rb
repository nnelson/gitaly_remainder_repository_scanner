# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

# GitalyManagement module
module GitalyManagement
  # Helper methods
  module Helpers
    ApplicationError = Class.new(StandardError)
    UserError = Class.new(StandardError)
    DENOMINATION_CONVERSIONS = {
      'Bytes': 1024,
      'KB': 1024 * 1024,
      'MB': 1024 * 1024 * 1024,
      'GB': 1024 * 1024 * 1024 * 1024,
      'TB': 1024 * 1024 * 1024 * 1024 * 1024
    }.freeze

    def human_friendly_filesize(bytes)
      DENOMINATION_CONVERSIONS.each_pair do |e, s|
        return [(bytes.to_f / (s / 1024)).round(2), e].join(' ') if bytes < s
      end
    end

    def percentage_of_total_disk_space(size, total_disk_space)
      return 0 if total_disk_space <= 0

      ((size / total_disk_space.to_f) * 100).round(2)
    end
  end
  # module Helpers
end
# module GitalyManagement
