# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

# GitalyManagement module
module GitalyManagement
  # Config module
  module Config
    DEFAULTS = {
      dry_run: true,
      repositories_root_dir_path: '/var/opt/gitlab/git-data/repositories',
      hashed_storage_dir_name: '@hashed',
      leftovers_dir_name: 'leftovers.d',
      leftovers_file_name: 'leftover-git-repositories-%<date>s.txt',
      log_level: Logger::INFO,
      env: :production,
      status: '%<index>s of %<total>s; %<percent>.2f%%',
      project_keys: [:id, :disk_path, :repository_storage],
      slice_size: 20,
      inventory_timestamp_format: '%Y-%m-%d_%H%M%S',
      log_timestamp_format: '%Y-%m-%d %H:%M:%S'
    }.freeze
  end
end
