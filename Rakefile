# -*- mode: ruby -*-
# vi: set ft=ruby :
# encoding: utf-8

require 'digest'
require 'rake'
require 'rake/clean'

load 'gitaly_remainder_repository_scanner.gemspec'

SOURCE_PATH = File.expand_path(File.dirname(__FILE__))
GEM_SPEC_FILENAME = "#{GEM_SPEC.name}.gemspec"
GEM_FILENAME = "#{GEM_SPEC.full_name}.gem"
# CLEAN.add File.join('lib', '**', '*.jar'), 'tmp'
CLOBBER.add '*.gem', 'pkg'

desc 'Resolve dependencies'
task :dependencies do
  # Include the extension configurator
  GEM_SPEC.extensions.each { |path| require_relative path }
  # Avoid invocation of the extension configurator upon user installation of gem
  GEM_SPEC.extensions.clear
end

desc 'Assemble the gem package'
task :package do
  require 'fileutils'
  package_dir_path = File.join('pkg', GEM_SPEC.full_name)
  FileUtils.mkdir_p package_dir_path
  FileUtils.cp_r 'ext', package_dir_path
  FileUtils.cp_r 'lib', package_dir_path
  FileUtils.cp ['LICENSE.txt', 'Rakefile', 'README.md'], package_dir_path
  package_gemspec_file_path = File.join(package_dir_path, GEM_SPEC_FILENAME)
  File.open(package_gemspec_file_path, 'w') do |io|
    io.write(GEM_SPEC.to_ruby)
  end
end
task package: :dependencies

desc 'Build the gem from the assembled package'
task :gem do
  package_dir_path = File.join('pkg', GEM_SPEC.full_name)
  Rake::Task[:package].invoke unless File.directory? package_dir_path
  gem_file_path = File.join(package_dir_path, GEM_FILENAME)
  Dir.chdir(package_dir_path) do
    system('gem', 'build', GEM_SPEC_FILENAME)
  end
  FileUtils.mv gem_file_path, SOURCE_PATH if File.exists? gem_file_path
end

# Unit tests
begin
  require 'rspec/core'
  require 'rspec/core/rake_task'
  RSpec.configure do |config|
    config.libs << 'lib' << 'spec'
  end

  desc 'Run unit tests'
  RSpec::Core::RakeTask.new(:spec) do |task|
    task.pattern = File.join('spec', 'rake', '*_spec.rb')
  end

  desc 'Run coverage'
  RSpec::Core::RakeTask.new(:rcov) do |task|
    task.rcov = true
    task.pattern = File.join('spec', 'rake', '*_spec.rb')
  end

  desc 'Run gem verification tests'
  RSpec::Core::RakeTask.new(:verify) do |task|
    gem_path = File.join(SOURCE_PATH, GEM_FILENAME)
    ENV['GEM_PATH'] = gem_path
    ENV['GEM_SPEC_FULL_NAME'] = GEM_SPEC.full_name
    Rake::Task[:gem].invoke unless File.exists? gem_path
    task.pattern = File.join('spec', 'verify', '*_spec.rb')
  end

  task :default => :spec
rescue LoadError
  # no rspec available
  puts "Detected missing dependencies. Please run: bundle install"
rescue StandardError => e
  puts "Unexpected error: #{e.message}"
end

desc 'Upload gem to https://rubygems.org/'
task :publish do
  gem_path = File.join(SOURCE_PATH, GEM_FILENAME)
  Rake::Task[:gem].invoke unless File.exists? gem_path
  system('gem', 'push', gem_path)
end
task publish: :verify
