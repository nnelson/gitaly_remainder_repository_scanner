# -*- mode: ruby -*-
# vi: set ft=ruby :
# encoding: utf-8

require_relative 'lib/gitaly_management/version.rb'

GEM_SPEC = Gem::Specification.new do |spec|
  spec.name        = 'gitaly-management-repository-scanner'
  spec.version     = GitalyManagement::RepositoryScanner.version
  spec.date        = '2019-08-28'
  spec.summary     = "A gitaly management tool to scan for remainder git " \
    "repositories."
  spec.description = "Scans a gitaly shard for repositories which have no " \
    "canonical residence record for that shard."
  spec.authors     = ['Nels Nelson']
  spec.email       = 'nnelson@gitlab.com'
  spec.files       = [
    Dir['*.gemspec'], Dir[File.join('lib', '**', '*.rb')],
    'LICENSE', 'Rakefile', 'README.md',
  ].flatten
  spec.bindir      = 'exe'
  spec.executables = Dir[File.join('exe', '*')]
  spec.platform    = 'ruby'
  spec.homepage    = 'https://rubygems.org/gems/gitaly-management-repository-scanner'
  spec.metadata    = {
    'source_code_uri': 'https://gitlab.com/nnelson/gitaly_management_repository_scanner',
    'bug_tracker_uri': 'https://gitlab.com/nnelson/gitaly_management_repository_scanner/issues'
  }
  spec.license     = 'MIT'

  spec.required_ruby_version = '>= 2.5.3'
  spec.add_development_dependency 'rake', '~> 12.3', '>= 12.3.2'
  spec.add_development_dependency 'rspec', '~> 3.8', '>= 3.8.0'
end
