# gitaly_remainder_repository_scanner

Scans a gitaly shard for repositories that have no correlating residence record for that shard.

Used to identify git repositories that were copied to other shards but not deleted.


## Setup

Install asdf:

```bash
source ~/.github
export asdf_release=$(curl --insecure --location --silent --show-error --header "Authorization: token ${GITHUB_OATH_PERSONAL_API_TOKEN}" --header "Accept: application/vnd.github.v3+json" "https://api.github.com/repos/asdf-vm/asdf/tags" | jq -r '.[0]["name"]')
export asdf_release="${asdf_release:-v0.8.1}"
git clone https://github.com/asdf-vm/asdf.git "${HOME}/.asdf" --branch "${asdf_release}"
pushd "${HOME}/.asdf"; git fetch origin; popd
source "${HOME}/.asdf/asdf.sh"; source "${HOME}/.asdf/completions/asdf.bash"
```

Install asdf ruby support:

```bash
asdf plugin add ruby
```

Install the project ruby:

```bash
git clone git@gitlab.com:gitlab-com/nnelson/gitaly_remainder_repository_scanner.git
cd gitaly_remainder_repository_scanner
asdf install ruby
```

Install the project dependencies:

```bash
gem install bundler
bundle install
bundle exec rake -T
```


## Building

To clean the project, run unit tests, build the gem file, and verify that the built artifact works, execute:

```bash
bundle exec rake clobber package spec gem verify
```


## Publish

To publish the gem, execute:

```bash
bundle exec rake publish
```


## Manifest

```bash
date -u; tree -I .git -I vendor
```

```
Tue 21 Sep 2021 04:32:40 PM UTC
.
├── Gemfile
├── Gemfile.lock
├── LICENSE
├── README.md
├── Rakefile
├── exe
│   ├── gitaly_management_repository_scanner
│   └── scan_for_remainder_repositories
├── gitaly_remainder_repository_scanner.gemspec
└── lib
    └── gitaly_management
        ├── cli.rb
        ├── config.rb
        ├── helpers.rb
        ├── logging.rb
        ├── repository_operator.rb
        ├── repository_scanner.rb
        └── version.rb

3 directories, 15 files
```
